class Cell
  attr_reader :state

  ALIVE = "alive"
  DEAD = "dead"

  def initialize(state)
    @state = state
  end

  def dead?
    @state == DEAD
  end

  def alive?
    @state == ALIVE
  end
end