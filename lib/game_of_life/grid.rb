class Grid
  attr_reader :state
  
  def initialize(state)
    @state = state
  end

  def output
    puts @state
  end
end