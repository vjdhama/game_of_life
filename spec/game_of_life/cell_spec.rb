require "spec_helper"

describe Cell do 
  describe "#dead?" do
    it "will check if cell is dead when initialzed dead" do 

      cell = Cell.new(Cell::DEAD)
      
      expect(cell).to be_dead
    end
  end

  describe '#alive?' do
    it "will check if cell is alive when initialized alive" do        
      cell = Cell.new(Cell::ALIVE)
      
      expect(cell).to be_alive

    end
  end
end
