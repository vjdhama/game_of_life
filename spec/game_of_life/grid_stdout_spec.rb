require 'spec_helper'

describe Grid do
  describe '#grid_stdout' do
    it "will print grid on STDOUT" do

      grid_state = [['*','0','0'],['0','*','0'],['0','0','*']]
      grid = Grid.new(grid_state)
      
      expect{grid.output}.to output.to_stdout
    end
  end
end